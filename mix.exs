defmodule DotenvParser.MixProject do
  use Mix.Project

  def project do
    [
      app: :dotenv_parser,
      version: "2.0.1",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: [
        main: DotenvParser,
        source_url_pattern:
          "https://git.ahlcode.fi/nicd/dotenv-parser/src/branch/trunk/%{path}#L%{line}"
      ],
      description: "Simple library to parse and load dotenv files.",
      package: [
        licenses: ["MIT", "BSD-2-Clause"],
        links: %{"Repository" => "https://git.ahlcode.fi/nicd/dotenv-parser"}
      ],
      source_url: "https://git.ahlcode.fi/nicd/dotenv-parser",
      homepage_url: "https://git.ahlcode.fi/nicd/dotenv-parser"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: []
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.34.2", only: :dev}
    ]
  end
end
